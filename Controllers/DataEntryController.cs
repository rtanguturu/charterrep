﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CharterReporting.Models;
using Microsoft.AspNetCore.Authorization;

namespace CharterReporting.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class DataEntryController : Controller
    {
        

        public IActionResult Index()
        {
            ViewData["Message"] = "Your Data Entry page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
